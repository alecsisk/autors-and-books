<?php
/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 09.07.2018
 * Time: 14:52
 */

namespace app\modules\Admin\models;


class Roles {
	const ROLE_ADMIN = 'admin';
	const ROLE_MODERATOR = 'moderator';
	const ROLE_USER = 'user';
	const ROLE_GUEST = 'guest';
}