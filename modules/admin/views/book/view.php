<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
	        [
		        'attribute' => 'autorsIds',
		        'format' => 'raw',
		        /** @var \app\models\Book $book */
		        'value' => function ($book) {

			        $autors = [];
			        /** @var \app\models\Autor $autor */
			        foreach ($book->getAutorsNames() as $autor) {
				        array_push($autors, Html::a($autor->getFullname(), ['autor/view', 'id' => $autor->id]));
			        }

			        return implode($autors, ', ');
		        }
	        ],
            'created_at:date',

        ],
    ]) ?>

</div>
