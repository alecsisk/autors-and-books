<?php

use app\models\Autor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'autorsIds')->widget(
		Select2::class, [
		'data' => ArrayHelper::map(Autor::find()->all(), 'id', 'fullname'),
		'options' => [
		        'placeholder' => 'Начните вводить имя автора...',
            'multiple' => true
        ],
		'pluginOptions' => [
			'allowClear' => false
		],
	]); ?>

    <div class="form-group">
		<?= Html::submitButton('Внести изменения', ['class' => 'btn btn-success']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
