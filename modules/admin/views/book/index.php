<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                    'attribute' => 'autorsIds',
                    'format' => 'raw',
                    /** @var \app\models\Book $book */
                    'value' => function ($book) {

                        $autors = [];
                        /** @var \app\models\Autor $autor */
	                    foreach ($book->getAutorsNames() as $autor) {
	                        array_push($autors, Html::a($autor->getFullname(), ['autor/view', 'id' => $autor->id]));
                        }

                        return implode($autors, ', ');
                    }
            ],
            'created_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
