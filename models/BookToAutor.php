<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_to_autor".
 *
 * @property int $id
 * @property int $book_id
 * @property int $autor_id
 */
class BookToAutor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_to_autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'autor_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'autor_id' => 'Autor ID',
        ];
    }
}
