<?php
/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 11.07.2018
 * Time: 17:30
 */

namespace app\modules\user\models\forms;


use app\modules\admin\models\User;
use Yii;
use yii\base\Model;

class SignupForm extends Model {
	public $email;
	public $password;


	/**
	 * @return array the validation rules.
	 */
	public function rules () {
		return [
			['email', 'trim'],
			[['email', 'password'], 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 50],
			['password', 'string', 'min' => 6],
			['email', 'unique','targetClass' => '\common\models\User', 'message' => 'Этот email уже используется']
		];
	}

	/**
	 * Logs in a user using the provided username and password.
	 * @return bool whether the user is logged in successfully
	 */
	public function save () {

		if ($this->validate()) {

			$user = new User();

			$user->email = $this->email;
			$user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
		}

		return false;
	}
}