<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180708_121752_create_user_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp () {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'email' => $this->string(50)->notNull()->unique(),
			'auth_key' => $this->string(32)->notNull(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string()->unique(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
		], $tableOptions);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown () {
		$this->dropTable('{{%user}}');
	}
}
