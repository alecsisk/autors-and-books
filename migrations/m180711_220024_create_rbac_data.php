<?php

use yii\db\Migration;

/**
 * Class m180711_220024_create_rbac_data
 */
class m180711_220024_create_rbac_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_220024_create_rbac_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180711_220024_create_rbac_data cannot be reverted.\n";

        return false;
    }
    */
}
