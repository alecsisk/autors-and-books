<?php

use yii\db\Migration;

/**
 * Handles the creation of table `book_to_autor`.
 */
class m180709_122720_create_book_to_autor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('book_to_autor', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('book_to_autor');
    }
}
