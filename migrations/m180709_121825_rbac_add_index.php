<?php

use yii\base\InvalidConfigException;
use yii\db\Migration;
use yii\rbac\DbManager;

/**
 * Class m180709_121825_rbac_add_index
 */
class m180709_121825_rbac_add_index extends Migration {
	public $column = 'user_id';
	public $index = 'auth_assignment_user_id_idx';

	/**
	 * @throws yii\base\InvalidConfigException
	 * @return DbManager
	 */
	protected function getAuthManager () {
		$authManager = Yii::$app->getAuthManager();
		if (!$authManager instanceof DbManager) {
			throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
		}

		return $authManager;
	}

	/**
	 * {@inheritdoc}
	 */
	public function up () {
		$authManager = $this->getAuthManager();
		$this->createIndex($this->index, $authManager->assignmentTable, $this->column);
	}

	/**
	 * {@inheritdoc}
	 */
	public function down () {
		$authManager = $this->getAuthManager();
		$this->dropIndex($this->index, $authManager->assignmentTable);
	}
}
